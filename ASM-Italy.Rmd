---
title: "Italy Data"
output: html_notebook
---

```{r}
library("dplyr")
library("ggplot2")
library(MCMCpack)
library(gridExtra)
library(dplyr)


data<-read.csv('winemag-data-130k-v2.csv')
data<-droplevels(na.omit(data))

filtered_wines_1<-read.csv('winemag-data-130k-v2.csv')
filtered_wines_1 <- droplevels(na.omit(filtered_wines_1))
filtered_wines = filter(filtered_wines_1, (country == "Italy"  & price < 20))


mean(filtered_wines$points)

#filtered_wines = filter(filtered_wines, (region_1 !="Sardinia"))
filtered_wines$points = filtered_wines$points +  rnorm(nrow(filtered_wines), 10^-4 ,10^-5)

 
counter = table(filtered_wines$region_1)
counter = as.data.frame(counter)
counter = counter[-1,]
counter = counter %>% filter(Freq > 3)
filtered_wines <- filtered_wines[filtered_wines$region_1 %in% counter$Var1, , drop = FALSE]


ggplot(filtered_wines) + geom_boxplot(aes(x = reorder(region_1, points, mean), points,
                               fill = reorder(region_1, points, mean)), show.legend=FALSE)

ggplot(filtered_wines, aes(x = reorder(region_1, region_1, length))) + stat_count()

ggplot(filtered_wines, aes(points)) + stat_bin()

compare_m_gibbs <- function(y, ind, maxiter = 5000)
{
  
  ### weakly informative priors
  a0 <- 2 ; b0 <- 1 ## tau_w hyperparameters
  eta0 <- 2 ; t0 <- 1 ## tau_b hyperparameters
  mu0<- 3 ; gamma0 <- 1/0.64
  ###
  
  ### starting values
  m <- nlevels(factor(ind))
  ybar <- theta <- tapply(y, ind, mean)
  tau_w <- mean(1 / tapply(y, ind, var)) ##within group precision
  print(tau_w)
  mu <- mean(theta)
  tau_b <- 1/var(theta) ##between group precision
  n_m <- tapply(y, ind, length)
  an <- a0 + sum(n_m)/2
  ###
  
  ### setup MCMC
  theta_mat <- matrix(0, nrow=maxiter, ncol=m)
  mat_store <- matrix(0, nrow=maxiter, ncol=3)
  ###
  
  ### MCMC algorithm
  for(s in 1:maxiter) 
  {
    
    # sample new values of the thetas
    for(j in 1:m) 
    {
      taun <- n_m[j] * tau_w + tau_b
      thetan <- (ybar[j] * n_m[j] * tau_w + mu * tau_b) / taun
      theta[j]<-rnorm(1, thetan, 1/sqrt(taun))
    }
    
    #sample new value of tau_w
    ss <- 0
    for(j in 1:m){
      ss <- ss + sum((y[ind == j] - theta[j])^2)
    }
    bn <- b0 + ss/2
    tau_w <- rgamma(1, an, bn)
    
    #sample a new value of mu
    gammam <- m * tau_b + gamma0
    mum <- (mean(theta) * m * tau_b + mu0 * gamma0) / gammam
    mu <- rnorm(1, mum, 1/ sqrt(gammam)) 
    
    # sample a new value of tau_b
    etam <- eta0 + m/2
    tm <- t0 + sum((theta-mu)^2)/2
    tau_b <- rgamma(1, etam, tm)
    
    #store results
    theta_mat[s,] <- theta
    mat_store[s, ] <- c(mu, tau_w, tau_b)
  }
  colnames(mat_store) <- c("mu", "tau_w", "tau_b")
  return(list(params = mat_store, theta = theta_mat))
}

filtered_wines$region_index <- as.numeric(factor(filtered_wines$region_1))


fit2 <- compare_m_gibbs(filtered_wines$points, filtered_wines$region_index)

apply(fit2$params, 2, mean)

apply(fit2$params, 2, mean)
apply(fit2$params, 2, sd)
mean(1/sqrt(fit2$params[, 3]))
sd(1/sqrt(fit2$params[, 3]))
theta_hat=apply(fit2$theta,2,mean)
result=data.frame(size = tapply(filtered_wines$points, filtered_wines$region_index, length), 
                  theta_hat = theta_hat)
result$region_final = row.names(result)
result = filter(result, theta_hat > 86.58)
result

```

```{r}
count(filtered_wines, region_1, sort = TRUE)
```


```{r}
require(ggplot2)
require(reshape2)
ggplot(data = result, aes(x=theta_hat, y=size)) + 
               geom_point(aes(colour=region_final), show.legend = FALSE) + xlab("Mean of Point") +  ylab("Number of Samples")
```